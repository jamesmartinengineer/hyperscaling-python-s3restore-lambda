#This is a hyper-scaling worker function created by James Martin @ sensis.com.au / james.martin@engineer.com / james.martin@envisian.com.au. 0457 046 174
#21/02/2020
#This will take an event and do some work with the data it receives in its payload, like running an OS functions or aws infrastructure/api functions

#This is a worker function for a hyperscaling lambda pipeline
# it is typically invoked by lambda-hyperscale-producer, which in turn might be called by lambda-hyperscale-batch.
#It is intended that this worker is changed out as the application requires. Its difficult to generalise beyond this point
# and consider this to be the thing that actually does the desireable result, although it might produce something else
# depending on requirement. The worker is the most unique and complicated part of the lambda hyperscaling pipeline

# Note: Do not handle function calls that might abend the worker. Don't try to handle failed worker threads as it makes it harder to monitor and log.

#Required Role: lambda-hyperscale-worker

#Test event:
#name: testRestore
#{
#  "commandToRun": "test-restore/",
#  "bucketName": "bucket-name"
#}

import json
import boto3
import os

def lambda_handler(event, context):
    # get command from event
    print("my command: " + event['commandToRun'])
    bucketName=event['bucketName']
    jobcode="Success"
    
    #### insert worker code below ####
    
    #This worker expects an S3 bucket prefix and will recover any deleted files (files with a delete marker)
    # The action to recover the file is a 'delete' which might seem counter-intuitive
    # the job will not delete any files and it can be safely run multiple times over the same folder
    #
    # This is built as a hyper-scaling worker so that it can deal with a very high volume of deleted files in multiple folders
    # in a very short time.
    
    #Variables:
    armed=True #arms/disarms the ability to undelete files. Otherwise restorable files will only be reported
    
    #Function:
    s3 = boto3.client('s3')
    s3res = boto3.resource('s3')
    #s3ObjectPrefix="test-delete/"
    s3ObjectPrefix=event['commandToRun']
    #get that folder prefix into a bucket object
    bucket= s3res.Bucket(bucketName)
    buckCollection= bucket.objects.filter(Prefix=s3ObjectPrefix)
    object_version_iterator = bucket.object_versions.filter(Prefix=s3ObjectPrefix)
    
    #This will pull the object versions from the specified prefix
    response = s3.list_object_versions(
        Bucket=bucketName,
        #Delimiter='string',
        #EncodingType='url',
        #KeyMarker='string',
        #MaxKeys=123,
        Prefix=s3ObjectPrefix,
        #VersionIdMarker='string'
    )
    #print(response['DeleteMarkers']) # testing
    
    #This will only iterate on versions with delete markers
    try:
        for marker in response['DeleteMarkers']:
            print(bucketName)
            print(marker['Key'])
            print(marker['VersionId'])
            
            #delete the delete marker versions
            processing =True
            num_retries=0
            max_retries=10
            min_sleep_time=0.05
            if armed==True:
                while processing==True:
                    try:
                        response = s3.delete_object(
                            Bucket=bucketName,
                            Key=marker['Key'],
                            #MFA='string',
                            VersionId=marker['VersionId'],
                            #RequestPayer='requester',
                            #BypassGovernanceRetention=True|False
                        )
                        print (str(response))
                        processing=False
                    except ClientError as err:
                        if "Rate exceeded" in err.args[0]:
                            # if we hit the retry limit, we'll go to sleep for a bit then try again.
                            # the number of retries determines our sleep time. This thread will sleep for
                            # min_sleep_time * random.randint(1, 2 ** num_retries), up to at most
                            # min_sleep_time * max_retries.
                            # After max_retries, we can't give up, so we scale back the number of retries by a random int
                            # to avoid collision with other threads.
                            num_retries += 1
                            if num_retries > max_retries:
                                num_retries = random.randint(1, self.max_retries)
                                processing=False
                                jobcode="Failed"
                            sleep_time = min_sleep_time * random.randint(1, 2 ** num_retries)
                            print("Hit retry limit, sleeping for " + str(sleep_time) + " seconds")
                            print(str(err))
                            time.sleep(sleep_time)
                        else:
                            # let the caller handle every other error.
                            print("Error: some unhandled error")
                            processing=False
                            jobcode="Failed"
                            #raise
            else:
                print("Disarmed. No changes made")
    except:
        print("No 'Delete Markers' found")
    #### end worker code ####
    
    result = "Job returned "+ jobcode
    return {
        'statusCode': 200,
        'body': json.dumps(result)
    }
