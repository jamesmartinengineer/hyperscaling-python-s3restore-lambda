#This is a hyper-scaling producer function created by James Martin @ sensis.com.au / james.martin@engineer.com / james.martin@envisian.com.au
#21/02/2020
#This will read a control file and then batch-invoke asychronous lambda functions to do some work

#This is a producer for lambda-hyperscale-worker
#lambda-hyperscale-producer invokes additional lambda-hyperscale-workers

#This lambda can be called from the lambda-hyperscale-batcher if this function invokes files too slowly

#This function can generally spin up ~1,000 lambda functions per minute (this is likely limited by
#time a single thread takes to talk to the AWS API), so if you need to produce faster, you need a batcher
#or to run more threads another way

#Required Role: lambda-hyperscale-producer

#Test event:
#name: manualRun
#{
#  "bucketName": "bucket-name",
#  "commandToRun": "restore-test.txt"
#}

import json
import boto3
import sys
import random
import time
from botocore.exceptions import ClientError

def lambda_handler(event, context):
    #get command from event
    print("my command: " + event['commandToRun'])
    print("my bucket: " + event['bucketName'])
    bucketName=event['bucketName']
    jobcode="Success"
        
    #### insert producer logic below ####
    #Variables:
    lambdaFnToRun="lambda-hyperscale-worker"
    
    #Function:
    #get file from S3 bucket
    s3 = boto3.client('s3')
    s3ObjectName=event['commandToRun']
    fileToCreate="/tmp/controlfile"
    #try:
        #s3.download_file('BUCKET_NAME', 'OBJECT_NAME', 'FILE_NAME')
    #just let it abend if the file is missing. Threads are disposable
    s3.download_file(bucketName, s3ObjectName, fileToCreate)
    #process file to list
    print("reading data")
    with open(fileToCreate) as f:
        lineList = f.readlines()
    counter=0
    lambdaFn = boto3.client('lambda')
    print("number of records " + str(len(lineList)))
    for eachFunction in lineList:
    #### end producer logic ####
        counter=counter+1
        #boto back-off and rety when invoking more functions
        processing =True
        num_retries=0
        max_retries=10
        min_sleep_time=0.05
        while processing==True:
            try:
                print("invoking lambda " + str(counter))
                payload3='{\n"commandToRun":"' + eachFunction.rstrip() + '",\n "bucketName":"' + bucketName.rstrip() +'"\n}'
                payload3=payload3.encode('utf-8')
                print(str(payload3))
                #Invoke Lambda with JSON payload
                lambdaFn.invoke(
                    FunctionName=lambdaFnToRun,
                    InvocationType='Event',
                    Payload=payload3
                )
                processing=False
            except ClientError as err:
                if "Rate exceeded" in err.args[0]:
                    # if we hit the retry limit, we'll go to sleep for a bit then try again.
                    # the number of retries determines our sleep time. This thread will sleep for
                    # min_sleep_time * random.randint(1, 2 ** num_retries), up to at most
                    # min_sleep_time * max_retries.
                    # After max_retries, we can't give up, so we scale back the number of retries by a random int
                    # to avoid collision with other threads.
                    num_retries += 1
                    if num_retries > max_retries:
                        num_retries = random.randint(1, self.max_retries)
                        processing=False
                        jobcode="Failed"
                    sleep_time = min_sleep_time * random.randint(1, 2 ** num_retries)
                    print("Hit retry limit, sleeping for " + str(sleep_time) + " seconds")
                    print(str(err))
                    time.sleep(sleep_time)
                else:
                    # let the caller handle every other error.
                    print("Error: some unhandled error")
                    processing=False
                    jobcode="Failed"
                    #raise
    result = "Job returned "+ jobcode
    return {
        'statusCode': 200,
        'body': json.dumps(result)
    }
