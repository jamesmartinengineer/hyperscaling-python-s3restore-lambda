#This is a hyper-scaling producer function created by James Martin @ sensis.com.au / james.martin@engineer.com / james.martin@envisian.com.au
#21/02/2020
#This will feed a list of control files and then batch-invoke asychronous lambda functions to do some work

#This is a batcher for lambda-hyperscale-producer
#lambda-hyperscale-producer invokes additional lambda-hyperscale-workers

#Required Role: lambda-hyperscale-producer

#Test event:
#name: manualRun
#{
#  "bucketName": "bucket-name"
#}

import json
import boto3
import sys
import random
import time
from botocore.exceptions import ClientError

def lambda_handler(event, context):
    #### Enter batch data and variables below ####
    lambdaFnToRun="lambda-hyperscale-producer"
    print("my bucket: " + event['bucketName'])
    bucketName=event['bucketName']
    fileList=["xaa",
        "xab",
        "xac",
        "xad",
        "xae",
        "xaf",
        "xag",
        "xah",
        "xai",
        "xaj",
        "xak",
        "xal",
        "xam",
        "xan",
        "xao",
        "xap",
        "xaq",
        "xar",
        "xas",
        "xat",
        "xau",
        "xav",
        "xaw",
        "xax",
        "xay",
        "xaz",
        "xba",
        "xbb",
        "xbc",
        "xbd",
        "xbe",
        "xbf",
        "xbg",
        "xbh",
        "xbi",]

    jobcode="Success"

    counter=0
    lambdaFn = boto3.client('lambda')

    #### end batch data ####
    for batch in fileList:
        counter=counter+1
        #print("Line "+ str(counter) + " " + eachFunction) 
        #boto back-off and rety
        processing =True
        num_retries=0
        max_retries=10
        min_sleep_time=0.05
        while processing==True:
            try:
                payload3='{"commandToRun": "' + batch.rstrip() + '"\n, "bucketName": "' + bucketName.rstrip() +'"\n}'
                payload3=payload3.encode('utf-8')
                print(payload3)
                ####Do lambda
                lambdaFn.invoke(
                    FunctionName=lambdaFnToRun,
                    InvocationType='Event',
                    Payload=payload3
                )
                print("invoking lambda " + str(counter))
                processing=False
            except ClientError as err:
                if "Rate exceeded" in err.args[0]:
                    # if we hit the retry limit", we'll go to sleep for a bit then try again.
                    # the number of retries determines our sleep time. This thread will sleep for
                    # min_sleep_time * random.randint(1", 2 ** num_retries)", up to at most
                    # min_sleep_time * max_retries.
                    # After max_retries", we can't give up", so we scale back the number of retries by a random int
                    # to avoid collision with other threads.
                    num_retries += 1
                    if num_retries > max_retries:
                        num_retries = random.randint(1, self.max_retries)
                        processing=False
                        jobcode="Failed"
                    sleep_time = min_sleep_time * random.randint(1, 2 ** num_retries)
                    print("Hit retry limit, sleeping for " + str(sleep_time) + " seconds")
                    print(str(err))
                    time.sleep(sleep_time)
                else:
                    # let the caller handle every other error.
                    print("Error: some unhandled error")
                    processing=False
                    jobcode="Failed"
                    #raise
    result = "Job returned "+ jobcode
    
    return {
        'statusCode': 200,
        'body': json.dumps(result)
    }
